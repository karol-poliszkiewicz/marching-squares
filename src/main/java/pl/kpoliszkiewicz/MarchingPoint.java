package pl.kpoliszkiewicz;

import javafx.scene.paint.Color;

record MarchingPoint(
        int x,
        int y,
        Color color
) {

    int value() {
        return Color.BLACK.equals(color) ? 1 : 0;
    }
}
