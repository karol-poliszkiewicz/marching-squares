package pl.kpoliszkiewicz;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class MarchingSquares extends Canvas {

    private final int scale;
    private MarchingPoint[][] points;

    MarchingSquares(int scale) {
        super(1024, 1024);
        this.scale = scale;
    }

    void setPoints(MarchingPoint[][] points) {
        this.points = null;
        this.points = points.clone();
    }

    void draw() {
        GraphicsContext gc = this.getGraphicsContext2D();
        gc.setFill(Color.DARKGRAY);
        gc.setLineWidth(4.0);
        for (int x = 0; x < points.length; x++) {
            for (int y = 0; y < points[0].length; y++) {
                int x1 = x * scale;
                int y1 = y * scale;
                gc.beginPath();
                gc.moveTo(x1, y1);
                gc.lineTo(x1, y1);
                gc.setStroke(points[x][y].color());
                gc.stroke();
                gc.closePath();
            }
        }
    }

    void process() {
        double offset = 0.5;
        for (int x = 0; x < points.length - 1; x++) {
            for (int y = 0; y < points[0].length - 1; y++) {

                int x2 = x + 1;
                int y2 = y + 1;
                String value = "" + points[x][y].value() + points[x + 1][y].value() + points[x + 1][y + 1].value() + points[x][y + 1].value();

                switch (value) {
                    case "0000", "1111" -> {}
                    case "1110", "0001" -> drawLine(x, y + offset, x + offset, y2);
                    case "1101", "0010" -> drawLine(x + offset, y2, x2, y + offset);
                    case "1011", "0100" -> drawLine(x + offset, y, x2, y + offset);
                    case "0111", "1000" -> drawLine(x, y + offset, x + offset, y);
                    case "1100", "0011" -> drawLine(x, y + offset, x2, y + offset);
                    case "1001", "0110" -> drawLine(x + offset, y, x + offset, y2);
                    case "1010" -> {
                        drawLine(x, y + offset, x + offset, y2);
                        drawLine(x + offset, y, x2, y + offset);
                    }
                    case "0101" -> {
                        drawLine(x, y + offset, x + offset, y);
                        drawLine(x + offset, y2, x2, y + offset);
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + value);
                }
            }
        }
    }

    void drawLine(double x1, double y1, double x2, double y2) {
        GraphicsContext gc = this.getGraphicsContext2D();
        gc.beginPath();
        gc.setLineWidth(.5);
        gc.setStroke(Color.RED);

        int scaledX1 = (int) (x1 * scale);
        int scaledY1 = (int) (y1 * scale);
        int scaledX2 = (int) (x2 * scale);
        int scaledY2 = (int) (y2 * scale);

        gc.moveTo(scaledX1, scaledY1);
        gc.lineTo(scaledX1, scaledY1);
        gc.lineTo(scaledX2, scaledY2);
        gc.stroke();
        gc.closePath();
    }

    void drawBorders() {
        int width = (int) getWidth() / scale;
        int height = (int) getHeight() / scale;
        drawLine(0, 0, width, 0);
        drawLine(width, 0, width, height);
        drawLine(width, height, 0, height);
        drawLine(0, height, 0, 0);
    }

    void clear() {
        GraphicsContext gc = this.getGraphicsContext2D();
        gc.clearRect(0, 0, getWidth(), getHeight());
    }
}
