package pl.kpoliszkiewicz;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

// https://en.wikipedia.org/wiki/Marching_squares
public class Main extends Application {

    private static final int SCALE = 8;
    private static final int SIZE = 256;

    @Override
    public void start(Stage stage) {
        Group root = new Group();
        int width = 1024;
        int height = 1024;
        Scene scene = new Scene(root, width, height);
        scene.setFill(Color.LIGHTGRAY);
        MarchingSquares marchingSquares = new MarchingSquares(SCALE);
        root.getChildren().add(marchingSquares);
        march(marchingSquares);
        scene.setOnMouseClicked(mouseEvent -> march(marchingSquares));
        stage.setScene(scene);
        stage.setTitle("Marching squares");
        stage.show();
    }

    private void march(MarchingSquares marchingSquares) {
        marchingSquares.clear();
        marchingSquares.setPoints(generatePoints());
        marchingSquares.draw();
        marchingSquares.process();
        marchingSquares.drawBorders();
    }

    private MarchingPoint[][] generatePoints() {
        MarchingPoint[][] points = new MarchingPoint[SIZE][SIZE];
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                points[x][y] = new MarchingPoint(x, y, Math.round(Math.random()) % 2 == 0 ? Color.WHITE : Color.BLACK);
            }
        }
        return points;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
