# Marching Squares

This is a Java application that implements the Marching Squares algorithm. The Marching Squares algorithm is a computer graphics algorithm that generates contours for a two-dimensional scalar field (rectangular array of individual numerical values). More details about the algorithm can be found on [Wikipedia](https://en.wikipedia.org/wiki/Marching_squares).

## Features

- Generates a grid of points with random colors (black or white).
- Processes the grid using the Marching Squares algorithm.
- Draws the resulting contours on the screen.
- Allows the user to regenerate the grid and the contours by clicking on the screen.

## Requirements

- Java 17
- Maven
- JavaFX 22

## Building and Running the Project

1. Clone the repository.
2. Navigate to the project directory.
3. Build the project using Maven: `mvn clean install`.
4. Run the application: `mvn javafx:run`.

## Code Structure

- `Main.java`: The entry point of the application. It sets up the JavaFX application and handles user interactions.
- `MarchingSquares.java`: This class implements the Marching Squares algorithm. It generates the contours and draws them on the screen.
- `MarchingPoint.java`: This class represents a point in the grid with a specific color.

## Contributing

Contributions are welcome. Please open an issue to discuss your idea or submit a pull request.

## License

This project is licensed under the terms of the MIT license.